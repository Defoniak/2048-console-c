It's a simple & small C project based on the original 2048. It's fully playable on the console with a high definition ASCII design.

I made it in a single day just for fun few years ago but i'm still pretty proud about it ! :)

![2048Console.jpg](https://bitbucket.org/repo/yLgyee/images/2177332381-2048Console.jpg)