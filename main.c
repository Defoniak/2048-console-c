#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int value;
    int move[4];/*0 = nord, 1 = est, 2 = sud, 3 = ouest and 0 = can't move // 1 = can move*/
} T_tuile;

typedef struct{
    T_tuile tuile[4][4];
} damier;

void drawBG(damier*);
int move(int, damier*);
int canMove(int, T_tuile*);
int moveCase(int, damier*, int, int);
void initDir(damier*);
int nbCase(damier*);
void popCase(damier*);
int rng(int, int);
void drawSc(int,int);
void saveScoreMax(int);
int loadScoreMax();
int gameOver(damier*);
void reset(damier*, int*);

int main()
{
    damier dam = {{0,{0,0,0,0}}};
    popCase(&dam);
    initDir(&dam);

    int c;
    int continuer = 1;
    int score = 0,scoreMax = loadScoreMax();

    drawSc(score,scoreMax);
    drawBG(&dam);

    while(continuer){
        if(gameOver(&dam)){
            if(score > scoreMax){
                scoreMax = score;
                saveScoreMax(scoreMax);
            }
            system("cls");
            drawSc(score,scoreMax);
            printf("\nGameOver\n");
            printf("press r to reset or esc to quit\n");
        }

        c = getch();
        switch(c){
            case 27: continuer = 0;
            break;
            case 75: score += move(3,&dam);
            break;
            case 77: score += move(1,&dam);
            break;
            case 80: score += move(2,&dam);
            break;
            case 72: score += move(0,&dam);
            break;
            case 114: reset(&dam,&score);
            break;
        }
        system("cls");
        drawSc(score,scoreMax);
        drawBG(&dam);
    }
    return 0;
}


void drawBG(damier* dam){
    int i,j;
    printf("+++++++++++++++++++++++++++++\n");
    for(i = 0; i < 4; i++){
        printf("+      +      +      +      +\n+");
        for(j = 0; j < 4; j++){
            if(dam->tuile[i][j].value != 0) printf(" %4d +",dam->tuile[i][j].value);
            else printf("      +");
        }
        printf("\n+      +      +      +      +\n");
        printf("+++++++++++++++++++++++++++++\n");
    }
    printf("press r to reset or esc to quit");
}
/*0 : can't move, 1 : can move, 2 : can merge*/
void initDir(damier* dam){
    int i,j;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            dam->tuile[i][j].move[0] = ((i-1 == -1) || ((dam->tuile[i-1][j].value != 0) && (dam->tuile[i-1][j].value != dam->tuile[i][j].value)))? 0: 1;
            dam->tuile[i][j].move[1] = ((j+1 == 4) || ((dam->tuile[i][j+1].value != 0) && (dam->tuile[i][j+1].value != dam->tuile[i][j].value)))? 0: 1;
            dam->tuile[i][j].move[2] = ((i+1 == 4) || ((dam->tuile[i+1][j].value != 0) && (dam->tuile[i+1][j].value != dam->tuile[i][j].value)))? 0: 1;
            dam->tuile[i][j].move[3] = ((j-1 == -1) || ((dam->tuile[i][j-1].value != 0) && (dam->tuile[i][j-1].value != dam->tuile[i][j].value)))? 0: 1;
        }
    }
}

int move(int dir, damier* dam){
    int i,j,i2,j2;
    int moved = 0;
    int point = 0;
    int score = 0;

    switch(dir){
        case 2:
            for(i = 3; i >= 0; i--){
                for(j = 0; j < 4; j++){
                    i2 = i;
                    j2 = j;
                    point = 0;
                    while(canMove(dir,&dam->tuile[i2][j2]) && point == 0){
                        moved = 1;
                        point = moveCase(dir,dam,i2,j2);
                        score += point;
                        i2++;
                    }
                }
            }
        break;
        case 1 :
            for(i = 0; i < 4; i++){
                for(j = 3; j >= 0; j--){
                    i2 = i;
                    j2 = j;
                    point = 0;
                    while(canMove(dir,&dam->tuile[i2][j2]) && point == 0){
                        moved = 1;
                        point = moveCase(dir,dam,i2,j2);
                        score += point;
                        j2++;
                    }
                }
            }
        break;
        default :
            for(i = 0; i < 4; i++){
                for(j = 0; j < 4; j++){
                    i2 = i;
                    j2 = j;
                    point = 0;
                    while(canMove(dir,&dam->tuile[i2][j2]) && point == 0){
                        moved = 1;
                        point = moveCase(dir,dam,i2,j2);
                        score += point;
                        switch(dir){
                            case 0 : i2--;
                            break;
                            case 3 : j2--;
                            break;
                        }
                    }
                }
            }
    }
    if(moved) popCase(dam);
    return score;
}


int canMove(int dir, T_tuile* tuile){
    return((tuile->move[dir]) && (tuile->value !=0));
}

int moveCase(int dir, damier* dam, int i, int j){
    int i2 = i,j2 = j;
    int point = 0;

    switch(dir){
        case 0: i2 = i-1;
        break;
        case 1: j2 = j+1;
        break;
        case 2: i2 = i+1;
        break;
        case 3: j2 = j-1;
        break;
    }

    if(dam->tuile[i][j].value == dam->tuile[i2][j2].value) {
        dam->tuile[i][j].value *= 2;
        point = dam->tuile[i][j].value;
    }
    dam->tuile[i2][j2].value = dam->tuile[i][j].value;
    dam->tuile[i][j].value = 0;
    initDir(dam);
    return point;
}

int nbCase(damier* dam){
    int i,j,nb;
    nb = 0;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            if(dam->tuile[i][j].value != 0) nb++;
        }
    }
    return 16-nb;
}

int rng(int a, int b){
    if(b>a){
        srand(time(NULL));
        return(rand()%(b-a) + a);
    }
    else return -1;
}

void popCase(damier* dam){
    int i,j;

    int pos = 0;
    int nb = nbCase(dam)+1;
    int random = rng(0,nb);
    if (random == -1) return;
    if (random == 16){
        dam->tuile[3][3].value = rng(1,2)*2;
        initDir(dam);
        return;
    }
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            if(dam->tuile[i][j].value == 0){
                if(pos == random) dam->tuile[i][j].value = rng(1,2)*2;
                pos++;
            }
        }
    }
    initDir(dam);
}

void drawSc(int score, int scoreMax){
    printf("++Score++  ++Sc.Max+++\n");
    printf("+       +  +         +\n");
    printf("+ %5d +  +  %5d  +\n", score, scoreMax);
    printf("+       +  +         +\n");
    printf("+++++++++  +++++++++++\n\n");
}

void saveScoreMax(int scoreMax){
    FILE *f = fopen("score.txt","wt");
    fprintf(f,"%d",scoreMax);
    fclose(f);
}

int loadScoreMax(){
    int scoreMax = 0;
    FILE *f = fopen("score.txt","rt");
    fscanf(f, "%d", &scoreMax);
    fclose(f);
    return scoreMax;
}
/*return 1 si plus de mouvement possible*/
int gameOver( damier* dam){
    int i,j,k;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            for(k = 0; k < 4; k++)
                if (dam->tuile[i][j].move[k] == 1) return 0;
        }
    }
    return 1;
}

void reset(damier* dam, int* score){
    int i,j,k;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 4; j++){
            dam->tuile[i][j].value = 0;
            for(k = 0; k < 4; k++)
                dam->tuile[i][j].move[k] = 0;
        }
    }
    *score = 0;
    popCase(dam);
}
